package info.city.soap.server;

import info.city.soap.model.Attraction;
import info.city.soap.util.AttractionUtil;

import javax.jws.WebMethod;
import javax.jws.WebService;
import java.util.ArrayList;

@WebService
public class CityInfo {
    @WebMethod
    public String[] getAllAttractions() {
        ArrayList<String> attractionArrayList = new ArrayList<>();
        ArrayList<Attraction> allAttractions = AttractionUtil.getAllAttractions();
        for (Attraction attraction : allAttractions) {
            attractionArrayList.add(attraction.getTitle());
        }
        return attractionArrayList.toArray(new String[0]);
    }

    @WebMethod
    public Attraction getAttractionByTitle(String title) {
        return AttractionUtil.getAttractionByName(title);
    }
}
