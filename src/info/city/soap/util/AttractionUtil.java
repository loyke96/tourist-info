package info.city.soap.util;

import com.google.gson.Gson;
import info.city.soap.model.Attraction;
import info.util.ErrorLogger;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.logging.Level;

public class AttractionUtil {
    static private ArrayList<Attraction> attractions;

    static {
        attractions = new ArrayList<>();
        try {
            File[] files = new File(System.getProperty("user.home") + File.separator + "CityAttractions").listFiles();
            Gson gson = new Gson();
            assert files != null;
            for (File file : files) {
                Attraction attraction = gson.fromJson(new FileReader(file), Attraction.class);
                attractions.add(attraction);
            }
        } catch (IOException e) {
            ErrorLogger.log(Level.SEVERE, e.getMessage());
        }
    }

    static public ArrayList<Attraction> getAllAttractions() {
        return attractions;
    }

    static public Attraction getAttractionByName(String title) {
        for (Attraction attraction : attractions) {
            if (attraction.getTitle().equals(title)) {
                return attraction;
            }
        }
        return null;
    }
}
