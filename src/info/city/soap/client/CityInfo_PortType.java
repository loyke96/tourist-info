/**
 * CityInfo_PortType.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package info.city.soap.client;

public interface CityInfo_PortType extends java.rmi.Remote {
    java.lang.String[] getAllAttractions() throws java.rmi.RemoteException;

    info.city.soap.client.Attraction getAttractionByTitle(java.lang.String title) throws java.rmi.RemoteException;
}
