/**
 * Attraction.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package info.city.soap.client;

public class Attraction implements java.io.Serializable {
    private java.lang.String description;

    private java.lang.String imageURL;

    private java.lang.String title;

    public Attraction() {
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
            new org.apache.axis.description.TypeDesc(Attraction.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://model.soap.city.info", "Attraction"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("description");
        elemField.setXmlName(new javax.xml.namespace.QName("http://model.soap.city.info", "description"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("imageURL");
        elemField.setXmlName(new javax.xml.namespace.QName("http://model.soap.city.info", "imageURL"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("title");
        elemField.setXmlName(new javax.xml.namespace.QName("http://model.soap.city.info", "title"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    private java.lang.Object __equalsCalc = null;
    private boolean __hashCodeCalc = false;


    public Attraction(
            java.lang.String description,
            java.lang.String imageURL,
            java.lang.String title) {
        this.description = description;
        this.imageURL = imageURL;
        this.title = title;
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
            java.lang.String mechType,
            java.lang.Class _javaType,
            javax.xml.namespace.QName _xmlType) {
        return
                new org.apache.axis.encoding.ser.BeanSerializer(
                        _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
            java.lang.String mechType,
            java.lang.Class _javaType,
            javax.xml.namespace.QName _xmlType) {
        return
                new org.apache.axis.encoding.ser.BeanDeserializer(
                        _javaType, _xmlType, typeDesc);
    }

    /**
     * Gets the description value for this Attraction.
     *
     * @return description
     */
    public java.lang.String getDescription() {
        return description;
    }

    /**
     * Sets the description value for this Attraction.
     *
     * @param description
     */
    public void setDescription(java.lang.String description) {
        this.description = description;
    }

    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getDescription() != null) {
            _hashCode += getDescription().hashCode();
        }
        if (getImageURL() != null) {
            _hashCode += getImageURL().hashCode();
        }
        if (getTitle() != null) {
            _hashCode += getTitle().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    /**
     * Gets the imageURL value for this Attraction.
     *
     * @return imageURL
     */
    public java.lang.String getImageURL() {
        return imageURL;
    }

    /**
     * Sets the imageURL value for this Attraction.
     *
     * @param imageURL
     */
    public void setImageURL(java.lang.String imageURL) {
        this.imageURL = imageURL;
    }

    /**
     * Gets the title value for this Attraction.
     *
     * @return title
     */
    public java.lang.String getTitle() {
        return title;
    }

    /**
     * Sets the title value for this Attraction.
     *
     * @param title
     */
    public void setTitle(java.lang.String title) {
        this.title = title;
    }

    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof Attraction)) return false;
        Attraction other = (Attraction) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true &&
                ((this.description == null && other.getDescription() == null) ||
                        (this.description != null &&
                                this.description.equals(other.getDescription()))) &&
                ((this.imageURL == null && other.getImageURL() == null) ||
                        (this.imageURL != null &&
                                this.imageURL.equals(other.getImageURL()))) &&
                ((this.title == null && other.getTitle() == null) ||
                        (this.title != null &&
                                this.title.equals(other.getTitle())));
        __equalsCalc = null;
        return _equals;
    }

}
