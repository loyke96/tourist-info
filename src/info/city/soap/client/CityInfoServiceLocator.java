/**
 * CityInfoServiceLocator.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package info.city.soap.client;

public class CityInfoServiceLocator extends org.apache.axis.client.Service implements info.city.soap.client.CityInfoService {

    public CityInfoServiceLocator() {
    }


    public CityInfoServiceLocator(org.apache.axis.EngineConfiguration config) {
        super(config);
    }

    public CityInfoServiceLocator(java.lang.String wsdlLoc, javax.xml.namespace.QName sName) throws javax.xml.rpc.ServiceException {
        super(wsdlLoc, sName);
    }

    // Use to get a proxy class for CityInfo
    private java.lang.String CityInfo_address = "http://localhost:8080//services/info/city/soap/server/CityInfo";

    public java.lang.String getCityInfoAddress() {
        return CityInfo_address;
    }

    // The WSDD service name defaults to the port name.
    private java.lang.String CityInfoWSDDServiceName = "CityInfo";

    public java.lang.String getCityInfoWSDDServiceName() {
        return CityInfoWSDDServiceName;
    }

    public void setCityInfoWSDDServiceName(java.lang.String name) {
        CityInfoWSDDServiceName = name;
    }

    public info.city.soap.client.CityInfo_PortType getCityInfo() throws javax.xml.rpc.ServiceException {
       java.net.URL endpoint;
        try {
            endpoint = new java.net.URL(CityInfo_address);
        }
        catch (java.net.MalformedURLException e) {
            throw new javax.xml.rpc.ServiceException(e);
        }
        return getCityInfo(endpoint);
    }

    public info.city.soap.client.CityInfo_PortType getCityInfo(java.net.URL portAddress) {
        try {
            info.city.soap.client.CityInfoSoapBindingStub _stub = new info.city.soap.client.CityInfoSoapBindingStub(portAddress, this);
            _stub.setPortName(getCityInfoWSDDServiceName());
            return _stub;
        }
        catch (org.apache.axis.AxisFault e) {
            return null;
        }
    }

    public void setCityInfoEndpointAddress(java.lang.String address) {
        CityInfo_address = address;
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        try {
            if (info.city.soap.client.CityInfo_PortType.class.isAssignableFrom(serviceEndpointInterface)) {
                info.city.soap.client.CityInfoSoapBindingStub _stub = new info.city.soap.client.CityInfoSoapBindingStub(new java.net.URL(CityInfo_address), this);
                _stub.setPortName(getCityInfoWSDDServiceName());
                return _stub;
            }
        }
        catch (java.lang.Throwable t) {
            throw new javax.xml.rpc.ServiceException(t);
        }
        throw new javax.xml.rpc.ServiceException("There is no stub implementation for the interface:  " + (serviceEndpointInterface == null ? "null" : serviceEndpointInterface.getName()));
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(javax.xml.namespace.QName portName, Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        if (portName == null) {
            return getPort(serviceEndpointInterface);
        }
        java.lang.String inputPortName = portName.getLocalPart();
        if ("CityInfo".equals(inputPortName)) {
            return getCityInfo();
        }
        else  {
            java.rmi.Remote _stub = getPort(serviceEndpointInterface);
            ((org.apache.axis.client.Stub) _stub).setPortName(portName);
            return _stub;
        }
    }

    public javax.xml.namespace.QName getServiceName() {
        return new javax.xml.namespace.QName("http://server.soap.city.info", "CityInfoService");
    }

    private java.util.HashSet ports = null;

    public java.util.Iterator getPorts() {
        if (ports == null) {
            ports = new java.util.HashSet();
            ports.add(new javax.xml.namespace.QName("http://server.soap.city.info", "CityInfo"));
        }
        return ports.iterator();
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(java.lang.String portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        
if ("CityInfo".equals(portName)) {
            setCityInfoEndpointAddress(address);
        }
        else 
{ // Unknown Port Name
            throw new javax.xml.rpc.ServiceException(" Cannot set Endpoint Address for Unknown Port" + portName);
        }
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(javax.xml.namespace.QName portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        setEndpointAddress(portName.getLocalPart(), address);
    }

}
