/**
 * CityInfoService.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package info.city.soap.client;

public interface CityInfoService extends javax.xml.rpc.Service {
    java.lang.String getCityInfoAddress();

    info.city.soap.client.CityInfo_PortType getCityInfo() throws javax.xml.rpc.ServiceException;

    info.city.soap.client.CityInfo_PortType getCityInfo(java.net.URL portAddress) throws javax.xml.rpc.ServiceException;
}
