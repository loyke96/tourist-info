package info.city.soap.client;

import info.util.ErrorLogger;

import javax.xml.rpc.ServiceException;
import java.rmi.RemoteException;
import java.util.logging.Level;

public class CityInfoClient {
    static private CityInfo_PortType portType;

    static {
        try {
            portType = new CityInfoServiceLocator().getCityInfo();
        } catch (ServiceException e) {
            ErrorLogger.log(Level.SEVERE, e.getMessage());
        }
    }

    static public String[] getAllAttractions() throws RemoteException {
        return portType.getAllAttractions();
    }

    static public Attraction getAttractionByTitle(String title) throws RemoteException {
        return portType.getAttractionByTitle(title);
    }
}
