package info.event.rest.client;

import info.event.rest.model.Event;

import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;

public class EventInfoClient {
    static private WebTarget target;

    static {
        target = ClientBuilder.newClient().target("http://localhost:8080/event/");
    }

    static public String[] getAll() {
        return target.path("/all")
                .request(MediaType.APPLICATION_JSON_TYPE)
                .get(String[].class);
    }

    static public String[] getByCategory(String category) {
        return target.path("/byCategory/" + category)
                .request(MediaType.APPLICATION_JSON_TYPE)
                .get(String[].class);
    }

    static public String[] getByDate(String date) {
        return target.path("/byDate/" + date)
                .request(MediaType.APPLICATION_JSON_TYPE)
                .get(String[].class);
    }

    static public String[] getByLocation(String location) {
        return target.path("/byLocation/" + location)
                .request(MediaType.APPLICATION_JSON_TYPE)
                .get(String[].class);
    }

    static public Event getEventByName(String fullName) {
        return target.path("/eventByName/" + fullName)
                .request(MediaType.APPLICATION_JSON_TYPE)
                .get(Event.class);
    }
}
