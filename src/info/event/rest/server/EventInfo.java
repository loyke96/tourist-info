package info.event.rest.server;

import info.event.rest.model.Event;
import info.event.rest.util.EventUtil;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import java.util.List;

@Path("/")
public class EventInfo {

    @GET
    @Path("/all")
    @Produces("application/json")
    public List<String> getAll() {
        return EventUtil.getAllEvents();
    }

    @GET
    @Path("/byCategory/{category}")
    @Produces("application/json")
    public List<String> getByCategory(@PathParam("category") String category) {
        return EventUtil.getByCategory(category);
    }

    @GET
    @Path("/byDate/{date}")
    @Produces("application/json")
    public List<String> getByDate(@PathParam("date") String date) {
        return EventUtil.getByDate(date);
    }

    @GET
    @Path("/byLocation/{location}")
    @Produces("application/json")
    public List<String> getByLocation(@PathParam("location") String location) {
        return EventUtil.getByLocation(location);
    }

    @GET
    @Path("/eventByName/{name}")
    @Produces("application/json")
    public Event getByName(@PathParam("name") String name) {
        return EventUtil.getEventByName(name);
    }
}
