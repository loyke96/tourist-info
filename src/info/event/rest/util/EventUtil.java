package info.event.rest.util;

import info.event.rest.model.Event;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;
import redis.clients.jedis.ScanParams;

import java.time.Duration;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.List;

public class EventUtil {
    static private Jedis jedis;
    static private ScanParams scanParams;

    static {
        JedisPool jedisPool = new JedisPool("localhost");
        jedis = jedisPool.getResource();
        scanParams = new ScanParams();
    }

    static public void addEvent(Event event) {
        HashMap<String, String> eventHM = new HashMap<>();
        eventHM.put("title", event.getTitle());
        eventHM.put("category", event.getCategory());
        String yyMMdd = event.getDate().format(DateTimeFormatter.ofPattern("yyMMdd"));
        eventHM.put("date", yyMMdd);
        eventHM.put("description", event.getDescription());
        eventHM.put("location", event.getLocation());

        String key = new StringBuilder()
                .append(event.getCategory())
                .append(':')
                .append(yyMMdd)
                .append(':')
                .append(event.getLocation())
                .append(':')
                .append(event.getTitle())
                .toString();

        jedis.hmset(key, eventHM);

        Duration expire = Duration.between(LocalDateTime.now(), event.getDate().atTime(23, 55));

        jedis.expire(key, (int) expire.getSeconds());
    }

    static public void cancelEvent(String title) {
        String scanResult = jedis.scan(ScanParams.SCAN_POINTER_START, scanParams.match("*:" + title)).getResult().get(0);
        jedis.expire(scanResult, 1);
    }

    static public List<String> getByCategory(String category) {
        List<String> result = jedis.scan(ScanParams.SCAN_POINTER_START,
                scanParams.match(category + ":*")).getResult();
        result.replaceAll(s ->
                s.substring(s.lastIndexOf(':') + 1)
        );
        return result;
    }

    static public List<String> getByLocation(String location) {
        List<String> result = jedis.scan(ScanParams.SCAN_POINTER_START,
                scanParams.match("*:" + location + ":*")).getResult();
        result.replaceAll(s ->
                s.substring(s.lastIndexOf(':') + 1)
        );
        return result;
    }

    static public List<String> getByDate(String date) {
        List<String> result = jedis.scan(ScanParams.SCAN_POINTER_START,
                scanParams.match("*:" + date + ":*")).getResult();
        result.replaceAll(s ->
                s.substring(s.lastIndexOf(':') + 1)
        );
        return result;
    }

    static public Event getEventByName(String name) {
        String scanResult = jedis.scan(ScanParams.SCAN_POINTER_START,
                scanParams.match("*:" + name)).getResult().get(0);
        List<String> hmget = jedis.hmget(scanResult, "title", "category", "date", "description", "location");
        return new Event(
                hmget.get(0),
                hmget.get(1),
                LocalDate.parse(hmget.get(2), DateTimeFormatter.ofPattern("yyMMdd")),
                hmget.get(3),
                hmget.get(4));
    }

    static public List<String> getAllEvents() {
        List<String> result = jedis.scan(ScanParams.SCAN_POINTER_START).getResult();
        result.replaceAll(s ->
                s.substring(s.lastIndexOf(':') + 1)
        );
        return result;
    }
}
