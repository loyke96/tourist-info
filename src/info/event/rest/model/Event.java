package info.event.rest.model;

import java.time.LocalDate;

public class Event {
    private String title;
    private String category;
    private LocalDate date;
    private String description;
    private String location;

    public Event() {
    }

    public Event(String title, String category, LocalDate date, String description, String location) {
        this.title = title;
        this.category = category;
        this.date = date;
        this.description = description;
        this.location = location;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public LocalDate getDate() {
        return date;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }
}
