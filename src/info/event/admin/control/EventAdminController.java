package info.event.admin.control;

import info.event.rest.model.Event;
import info.event.rest.util.EventUtil;
import javafx.collections.FXCollections;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.DatePicker;
import javafx.scene.control.ListView;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;

import java.net.URL;
import java.util.ResourceBundle;

public class EventAdminController implements Initializable {
    @FXML
    private ListView<String> eventsListView;
    @FXML
    private TextField titleTextField;
    @FXML
    private DatePicker datePicker;
    @FXML
    private TextArea descriptionTextArea;
    @FXML
    private TextField categoryTextField;
    @FXML
    private TextField locationTextField;


    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        eventsListView.setItems(FXCollections.observableArrayList(EventUtil.getAllEvents()));
    }

    @FXML
    private void onSelectEvent() {
        String selectedItem = eventsListView.getSelectionModel().getSelectedItem();
        if (selectedItem != null) {
            Event event = EventUtil.getEventByName(selectedItem);
            titleTextField.setText(event.getTitle());
            datePicker.setValue(event.getDate());
            descriptionTextArea.setText(event.getDescription());
            categoryTextField.setText(event.getCategory());
            locationTextField.setText(event.getLocation());
        }
    }

    @FXML
    private void onCancelEventClick() {
        String selectedItem = eventsListView.getSelectionModel().getSelectedItem();
        if (selectedItem != null) {
            EventUtil.cancelEvent(selectedItem);
            eventsListView.getItems().remove(selectedItem);
        }
        clearFields();
    }

    @FXML
    private void onAddEventClick() {
        String title = titleTextField.getText();
        Event event = new Event(
                title,
                categoryTextField.getText(),
                datePicker.getValue(),
                descriptionTextArea.getText(),
                locationTextField.getText()
        );
        EventUtil.addEvent(event);
        eventsListView.getItems().add(title);
        clearFields();
    }

    private void clearFields() {
        titleTextField.clear();
        categoryTextField.clear();
        datePicker.setValue(null);
        descriptionTextArea.clear();
        locationTextField.clear();
    }
}
