package info.weather.rest.client;

import info.util.ErrorLogger;
import info.weather.rest.model.Weather;

import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import java.io.FileReader;
import java.util.Properties;
import java.util.logging.Level;

public class WeatherClient {
    static private WebTarget webTarget;

    static {
        Properties properties = new Properties();
        try {
            properties.load(new FileReader("res/weather.properties"));
            String locationKey = properties.getProperty("LOCATION_KEY");
            String apiKey = properties.getProperty("API_KEY");
            String language = properties.getProperty("LANGUAGE");
            webTarget = ClientBuilder.newClient().target(
                    "https://dataservice.accuweather.com/currentconditions/v1/" +
                            locationKey + "?apikey=" + apiKey + "&language=" + language);
        } catch (Throwable t) {
            ErrorLogger.log(Level.SEVERE, t.getMessage());
        }
    }

    static public Weather getWeather() {
        return webTarget.request(MediaType.APPLICATION_JSON).get(Weather[].class)[0];
    }
}
