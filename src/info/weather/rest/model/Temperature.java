
package info.weather.rest.model;


public class Temperature {

    private Metric metric;
    private Imperial imperial;

    /**
     * No args constructor for use in serialization
     */
    public Temperature() {
    }

    /**
     * @param imperial
     * @param metric
     */
    public Temperature(Metric metric, Imperial imperial) {
        super();
        this.metric = metric;
        this.imperial = imperial;
    }

    public Metric getMetric() {
        return metric;
    }

    public void setMetric(Metric metric) {
        this.metric = metric;
    }

    public Imperial getImperial() {
        return imperial;
    }

    public void setImperial(Imperial imperial) {
        this.imperial = imperial;
    }

}
