
package info.weather.rest.model;


public class Metric {

    private double value;
    private String unit;
    private int unitType;

    /**
     * No args constructor for use in serialization
     */
    public Metric() {
    }

    /**
     * @param unit
     * @param unitType
     * @param value
     */
    public Metric(double value, String unit, int unitType) {
        super();
        this.value = value;
        this.unit = unit;
        this.unitType = unitType;
    }

    public double getValue() {
        return value;
    }

    public void setValue(double value) {
        this.value = value;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    public int getUnitType() {
        return unitType;
    }

    public void setUnitType(int unitType) {
        this.unitType = unitType;
    }

}
