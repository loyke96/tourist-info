
package info.weather.rest.model;


public class Weather {

    private String localObservationDateTime;
    private long epochTime;
    private String weatherText;
    private int weatherIcon;
    private boolean isDayTime;
    private Temperature temperature;
    private String mobileLink;
    private String link;

    /**
     * No args constructor for use in serialization
     */
    public Weather() {
    }

    /**
     * @param mobileLink
     * @param weatherIcon
     * @param weatherText
     * @param link
     * @param localObservationDateTime
     * @param epochTime
     * @param isDayTime
     * @param temperature
     */
    public Weather(String localObservationDateTime, long epochTime, String weatherText, int weatherIcon, boolean isDayTime, Temperature temperature, String mobileLink, String link) {
        super();
        this.localObservationDateTime = localObservationDateTime;
        this.epochTime = epochTime;
        this.weatherText = weatherText;
        this.weatherIcon = weatherIcon;
        this.isDayTime = isDayTime;
        this.temperature = temperature;
        this.mobileLink = mobileLink;
        this.link = link;
    }

    public String getLocalObservationDateTime() {
        return localObservationDateTime;
    }

    public void setLocalObservationDateTime(String localObservationDateTime) {
        this.localObservationDateTime = localObservationDateTime;
    }

    public long getEpochTime() {
        return epochTime;
    }

    public void setEpochTime(long epochTime) {
        this.epochTime = epochTime;
    }

    public String getWeatherText() {
        return weatherText;
    }

    public void setWeatherText(String weatherText) {
        this.weatherText = weatherText;
    }

    public int getWeatherIcon() {
        return weatherIcon;
    }

    public void setWeatherIcon(int weatherIcon) {
        this.weatherIcon = weatherIcon;
    }

    public boolean isIsDayTime() {
        return isDayTime;
    }

    public void setIsDayTime(boolean isDayTime) {
        this.isDayTime = isDayTime;
    }

    public Temperature getTemperature() {
        return temperature;
    }

    public void setTemperature(Temperature temperature) {
        this.temperature = temperature;
    }

    public String getMobileLink() {
        return mobileLink;
    }

    public void setMobileLink(String mobileLink) {
        this.mobileLink = mobileLink;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

}
