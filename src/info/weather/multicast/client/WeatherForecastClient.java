package info.weather.multicast.client;

import info.util.ErrorLogger;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.InetAddress;
import java.net.MulticastSocket;
import java.nio.charset.StandardCharsets;
import java.util.logging.Level;

public class WeatherForecastClient {
    static private MulticastSocket multicastSocket;
    static private DatagramPacket datagramPacket;

    static {
        try {
            byte[] buffer = new byte[32];
            multicastSocket = new MulticastSocket(9002);
            multicastSocket.joinGroup(InetAddress.getByName("225.8.8.3"));
            datagramPacket = new DatagramPacket(buffer, buffer.length);
        } catch (Throwable t) {
            ErrorLogger.log(Level.SEVERE, t.getMessage());
        }
    }

    static public String readForecast() throws IOException {
        multicastSocket.receive(datagramPacket);
        return new String(datagramPacket.getData(), 0,
                datagramPacket.getLength(), StandardCharsets.UTF_8);
    }
}
