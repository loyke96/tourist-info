package info.weather.multicast.server;

import info.util.ErrorLogger;
import info.weather.rest.client.WeatherClient;

import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.nio.charset.StandardCharsets;
import java.util.logging.Level;

public class WeatherForecast {
    public static void main(String[] args) {
        try {
            DatagramSocket datagramSocket = new DatagramSocket();
            DatagramPacket datagramPacket = new DatagramPacket(new byte[0], 0,
                    InetAddress.getByName("225.8.8.3"), 9002);
            while (true) {
                datagramPacket.setData(WeatherClient.getWeather().getWeatherText().getBytes(StandardCharsets.UTF_8));
                datagramSocket.send(datagramPacket);
                String value = String.valueOf(WeatherClient.getWeather().getTemperature().getMetric().getValue());
                datagramPacket.setData(value.getBytes(StandardCharsets.UTF_8));
                datagramSocket.send(datagramPacket);
                Thread.sleep(60_000);
            }
        } catch (Throwable t) {
            ErrorLogger.log(Level.SEVERE, t.getMessage());
        }
    }
}
