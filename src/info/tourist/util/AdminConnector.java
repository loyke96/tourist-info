package info.tourist.util;

import java.io.*;
import java.net.Socket;

public class AdminConnector {
    static private Socket socket;
    static private BufferedReader reader;
    static private PrintWriter writer;

    static public void connect() {
        try {
            socket = new Socket("localhost", 9003);
            reader = new BufferedReader(new InputStreamReader(socket.getInputStream()));
            writer = new PrintWriter(new OutputStreamWriter(socket.getOutputStream()), true);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    static public void logConnectionError(String remoteApp) {
        writer.println("Komunikacija sa aplikacijom " + remoteApp + " nije moguća.");
    }

    static public void logInternError(String file) {
        writer.println("Resurs " + file + " nije dostupan.");
    }

    static public void waitForShutDown() {
        try {
            while (!reader.readLine().equals("Shut_Down")) ;
            System.exit(0);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
