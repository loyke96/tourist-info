package info.tourist.util;

import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

import java.io.IOException;

public class StageLoader {

    static public void loadStage(String stageName, String title) {
        try {
            Parent root = FXMLLoader.load(StageLoader.class.getResource("/info/tourist/view/" + stageName + ".fxml"));
            Scene scene = new Scene(root);
            Stage stage = new Stage();
            stage.setScene(scene);
            stage.sizeToScene();
            stage.setResizable(false);
            stage.setTitle("Turist info :: " + title);
            stage.show();
        } catch (IOException e) {
            AdminConnector.logInternError(stageName + ".fxml");
        }
    }
}
