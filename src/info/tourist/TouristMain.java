package info.tourist;

import info.tourist.util.AdminConnector;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

import java.io.IOException;

public class TouristMain extends Application {

    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) throws IOException {
        AdminConnector.connect();
        Thread waitForShutDown = new Thread(AdminConnector::waitForShutDown);
        waitForShutDown.setDaemon(true);
        waitForShutDown.start();
        Parent root = FXMLLoader.load(getClass().getResource("/info/tourist/view/Home.fxml"));
        Scene scene = new Scene(root);
        primaryStage.setScene(scene);
        primaryStage.sizeToScene();
        primaryStage.setResizable(false);
        primaryStage.setTitle("Turist info :: Početna");
        primaryStage.show();
    }
}
