package info.tourist.control;

import info.bus.sockets.client.BusInfoClient;
import info.bus.sockets.model.Bus;
import info.bus.sockets.model.BusRealTime;
import info.bus.sockets.model.Relation;
import info.tourist.util.AdminConnector;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.ListView;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextArea;
import javafx.scene.control.cell.PropertyValueFactory;

import java.net.URL;
import java.util.HashMap;
import java.util.ResourceBundle;

public class BusesController implements Initializable {
    @FXML
    private TableView<Bus> busesTableView;
    @FXML
    private ListView<String> busStopsListView;
    @FXML
    private ListView<String> departureTimesListView;
    @FXML
    private TextArea realTimeInfoTextArea;

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        ObservableList<TableColumn<Bus, ?>> columns = busesTableView.getColumns();
        columns.get(0).setCellValueFactory(new PropertyValueFactory<>("number"));
        columns.get(1).setCellValueFactory(new PropertyValueFactory<>("relationFrom"));
        columns.get(2).setCellValueFactory(new PropertyValueFactory<>("relationTo"));
        try {
            HashMap<Integer, Relation> routes = BusInfoClient.getRoutes();
            routes.forEach((o, o2) -> busesTableView.getItems().add(new Bus(o, o2, null, null)));
        } catch (Throwable throwable) {
            AdminConnector.logConnectionError("Bus Info");
        }
    }

    @FXML
    private void onBusSelect() {
        Bus selectedItem = busesTableView.getSelectionModel().getSelectedItem();
        if (selectedItem != null) {
            try {
                BusRealTime realTimeBus = BusInfoClient.getRealTimeBus(selectedItem.getNumber());
                busStopsListView.setItems(FXCollections.observableArrayList(realTimeBus.getBus().getStations()));
                departureTimesListView.setItems(FXCollections.observableArrayList(realTimeBus.getBus().getDepartureTimes()));
                realTimeInfoTextArea.setText("Trenutna lokacija : " + realTimeBus.getLocation() + System.lineSeparator() + System.lineSeparator() +
                        "Dolazak na sledeću stanicu za " + realTimeBus.getArrivingIn() + " minuta.");
            } catch (Throwable throwable) {
                AdminConnector.logConnectionError("Bus Info");
            }
        }
    }
}
