package info.tourist.control;

import info.event.rest.client.EventInfoClient;
import info.event.rest.model.Event;
import info.tourist.util.AdminConnector;
import javafx.collections.FXCollections;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;

import java.net.URL;
import java.time.format.DateTimeFormatter;
import java.util.ResourceBundle;

public class EventsController implements Initializable {
    @FXML
    private ChoiceBox<String> searchChoiceBox;
    @FXML
    private TextField searchTermTextField;
    @FXML
    private DatePicker datePicker;
    @FXML
    private ListView<String> eventsListView;
    @FXML
    private TextField titleTextField;
    @FXML
    private TextField locationTextField;
    @FXML
    private TextField eventDateTextField;
    @FXML
    private TextArea descriptionTextArea;

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        try {
            searchChoiceBox.getItems().addAll("Kategorija", "Datum", "Lokacija");
            searchChoiceBox.getSelectionModel().selectedItemProperty().addListener((observableValue, s, t1) -> {
                if (t1.equals("Datum")) {
                    datePicker.setVisible(true);
                    searchTermTextField.setVisible(false);
                } else {
                    datePicker.setVisible(false);
                    searchTermTextField.setVisible(true);
                    searchTermTextField.setPromptText(t1);
                }
            });
            String[] items = EventInfoClient.getAll();
            eventsListView.setItems(FXCollections.observableArrayList(items));
            eventsListView.getSelectionModel().selectedItemProperty().addListener((observableValue, s, t1) -> selectEvent(t1));
        } catch (RuntimeException e) {
            AdminConnector.logConnectionError("Event Info");
        }
    }

    @FXML
    private void onSearchButtonClick() {
        try {
            if (searchChoiceBox.getValue().equals("Kategorija")) {
                eventsListView.setItems(FXCollections.observableArrayList(EventInfoClient.getByCategory(searchTermTextField.getText())));
            } else if (searchChoiceBox.getValue().equals("Datum")) {
                String yyMMdd = datePicker.getValue().format(DateTimeFormatter.ofPattern("yyMMdd"));
                eventsListView.setItems(FXCollections.observableArrayList(EventInfoClient.getByDate(yyMMdd)));
            } else if (searchChoiceBox.getValue().equals("Lokacija")) {
                eventsListView.setItems(FXCollections.observableArrayList(EventInfoClient.getByLocation(searchTermTextField.getText())));
            }
        } catch (RuntimeException e) {
            AdminConnector.logConnectionError("Event Info");
        }
    }

    @FXML
    private void selectEvent(String t1) {
        try {
            if (t1 != null) {
                Event event = EventInfoClient.getEventByName(t1);
                titleTextField.setText(event.getTitle());
                descriptionTextArea.setText(event.getDescription());
                locationTextField.setText(event.getLocation());
                eventDateTextField.setText(event.getDate().format(DateTimeFormatter.ofPattern("dd. MMM yyyy")));
            } else {
                titleTextField.clear();
                descriptionTextArea.clear();
                locationTextField.clear();
                eventDateTextField.clear();
            }
        } catch (RuntimeException e) {
            AdminConnector.logConnectionError("Event Info");
        }
    }
}
