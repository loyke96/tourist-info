package info.tourist.control;

import info.hotel.rmi.client.HotelClient;
import info.hotel.rmi.model.Hotel;
import info.tourist.util.AdminConnector;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;

import java.net.URL;
import java.rmi.RemoteException;
import java.util.ResourceBundle;

public class HotelsController implements Initializable {
    @FXML
    private TableView<Hotel> hotelsTableView;

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        ObservableList<TableColumn<Hotel, ?>> columns = hotelsTableView.getColumns();
        columns.get(0).setCellValueFactory(new PropertyValueFactory<>("title"));
        columns.get(1).setCellValueFactory(new PropertyValueFactory<>("address"));
        columns.get(2).setCellValueFactory(new PropertyValueFactory<>("phone"));
        columns.get(3).setCellValueFactory(new PropertyValueFactory<>("category"));
        try {
            HotelClient.getAllHotels().forEach(hotel -> hotelsTableView.getItems().add(hotel));
        } catch (RemoteException e) {
            AdminConnector.logConnectionError("Hotel Info");
        }
    }
}
