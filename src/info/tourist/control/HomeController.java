package info.tourist.control;

import info.tourist.util.StageLoader;
import info.util.ErrorLogger;
import info.weather.multicast.client.WeatherForecastClient;
import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;

import java.io.IOException;
import java.net.URL;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.ResourceBundle;
import java.util.Timer;
import java.util.TimerTask;
import java.util.logging.Level;

public class HomeController implements Initializable {
    @FXML
    private Label timeLabel;
    @FXML
    private Label weatherLabel;
    @FXML
    private Label temperatureLabel;

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        Thread timeThread = new Thread(() -> new Timer(true).scheduleAtFixedRate(new TimerTask() {
            @Override
            public void run() {
                Platform.runLater(() ->
                        timeLabel.setText(LocalTime.now().format(DateTimeFormatter.ofPattern("HH:mm:ss")))
                );
            }
        }, 0, 1_000));
        Thread weatherThread = new Thread(() -> {
            try {
                while (true) {
                    String wDesc = WeatherForecastClient.readForecast();
                    String wTemp = WeatherForecastClient.readForecast();
                    Platform.runLater(() -> {
                        weatherLabel.setText(wDesc);
                        temperatureLabel.setText(wTemp + " ℃");
                    });
                }
            } catch (IOException e) {
                ErrorLogger.log(Level.SEVERE, e.getMessage());
            }
        });
        timeThread.setDaemon(true);
        weatherThread.setDaemon(true);
        timeThread.start();
        weatherThread.start();
    }

    @FXML
    private void onAttractionsButtonClick() {
        StageLoader.loadStage("Attractions", "Atrakcije");
    }

    @FXML
    private void onHotelsButtonClick() {
        StageLoader.loadStage("Hotels", "Hoteli");
    }

    @FXML
    private void onBusesButtonClick() {
        StageLoader.loadStage("Buses", "Autobuske linije");
    }

    @FXML
    private void onEventsButtonClick() {
        StageLoader.loadStage("Events", "Događaji");
    }
}
