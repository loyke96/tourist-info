package info.tourist.control;

import info.city.soap.client.Attraction;
import info.city.soap.client.CityInfoClient;
import info.tourist.util.AdminConnector;
import javafx.collections.FXCollections;
import javafx.embed.swing.SwingFXUtils;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.rmi.RemoteException;
import java.util.ResourceBundle;

public class AttractionsController implements Initializable {
    @FXML
    private ListView<String> attractionsListView;
    @FXML
    private ImageView attractionImageView;
    @FXML
    private Label titleLabel;
    @FXML
    private Label descriptionLabel;

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        try {
            String[] allAttractions = CityInfoClient.getAllAttractions();
            attractionsListView.setItems(FXCollections.observableArrayList(allAttractions));
        } catch (RemoteException e) {
            AdminConnector.logConnectionError("City Info");
        }
    }

    @FXML
    private void onAttractionSelect() {
        String selectedItem = attractionsListView.getSelectionModel().getSelectedItem();
        if (selectedItem != null) {
            try {
                Attraction attraction = CityInfoClient.getAttractionByTitle(selectedItem);
                titleLabel.setText(attraction.getTitle());
                descriptionLabel.setText(attraction.getDescription());
                String imageURL = attraction.getImageURL();
                String extension = imageURL.substring(imageURL.length() - 3);
                File cache = new File("CachedImages" + File.separatorChar +
                        Integer.toHexString(imageURL.hashCode()) + '.' + extension);
                if (cache.exists()) {
                    attractionImageView.setImage(new Image(cache.toURI().toString()));
                } else {
                    Image image = new Image(imageURL);
                    BufferedImage bufferedImage = SwingFXUtils.fromFXImage(image, null);
                    ImageIO.write(bufferedImage, extension, cache);
                    attractionImageView.setImage(image);
                }
            } catch (IOException e) {
                AdminConnector.logConnectionError("City Info");
            }
        }
    }
}
