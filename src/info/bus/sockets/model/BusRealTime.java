package info.bus.sockets.model;

import java.io.Serializable;

public class BusRealTime implements Serializable {
    private Bus bus;
    private String location;
    private int arrivingIn;

    public BusRealTime() {
    }

    public BusRealTime(Bus bus, String location, int arrivingIn) {
        this.bus = bus;
        this.location = location;
        this.arrivingIn = arrivingIn;
    }

    public Bus getBus() {
        return bus;
    }

    public void setBus(Bus bus) {
        this.bus = bus;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public int getArrivingIn() {
        return arrivingIn;
    }

    public void setArrivingIn(int arrivingIn) {
        this.arrivingIn = arrivingIn;
    }
}
