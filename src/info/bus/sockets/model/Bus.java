package info.bus.sockets.model;

import java.io.Serializable;
import java.util.ArrayList;

public class Bus implements Serializable {
    private int number;
    private Relation relation;
    private ArrayList<String> stations;
    private ArrayList<String> departureTimes;

    public Bus() {
    }

    public Bus(int number, Relation relation, ArrayList<String> stations, ArrayList<String> departureTimes) {
        this.number = number;
        this.relation = relation;
        this.stations = stations;
        this.departureTimes = departureTimes;
    }

    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }

    public ArrayList<String> getStations() {
        return stations;
    }

    public void setStations(ArrayList<String> stations) {
        this.stations = stations;
    }

    public ArrayList<String> getDepartureTimes() {
        return departureTimes;
    }

    public void setDepartureTimes(ArrayList<String> departureTimes) {
        this.departureTimes = departureTimes;
    }

    public Relation getRelation() {
        return relation;
    }

    public void setRelation(Relation relation) {
        this.relation = relation;
    }

    public String getRelationFrom() {
        return relation.getFrom();
    }

    public String getRelationTo() {
        return relation.getTo();
    }
}
