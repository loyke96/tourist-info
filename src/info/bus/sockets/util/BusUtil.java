package info.bus.sockets.util;

import com.google.gson.Gson;
import info.bus.sockets.model.Bus;
import info.bus.sockets.model.BusRealTime;
import info.bus.sockets.model.Relation;
import info.util.ErrorLogger;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.HashMap;
import java.util.Random;
import java.util.logging.Level;

public class BusUtil {
    static private HashMap<Integer, BusRealTime> busRealTimeHashMap;
    static private Random random;

    static {
        random = new Random();
        busRealTimeHashMap = new HashMap<>();
        Gson gson = new Gson();
        File busDir = new File("BusRoutes");
        BusRealTime busRealTime;
        File[] busRouteFiles = busDir.listFiles();
        for (File file : busRouteFiles) {
            try {
                Bus fromJson = gson.fromJson(new FileReader(file), Bus.class);
                busRealTime = new BusRealTime(fromJson, null, 0);
                busRealTimeHashMap.put(fromJson.getNumber(), busRealTime);
            } catch (FileNotFoundException e) {
                ErrorLogger.log(Level.SEVERE, e.getMessage());
            }
        }
    }

    static public HashMap<Integer, Relation> getAllBusRoutes() {
        HashMap<Integer, Relation> integerDestinationHashMap = new HashMap<>();
        busRealTimeHashMap.forEach((integer, busRealTime) -> {
            integerDestinationHashMap.put(integer, busRealTime.getBus().getRelation());
        });
        return integerDestinationHashMap;
    }

    static public Bus getBusByNumber(int number) {
        return busRealTimeHashMap.get(number).getBus();
    }

    static public BusRealTime getRealTimeBusInfo(int number) {
        try {
            BusRealTime busRealTime = busRealTimeHashMap.get(number);
            int size = busRealTime.getBus().getStations().size();
            int nextInt = random.nextInt(size);
            busRealTime.setLocation(busRealTime.getBus().getStations().get(nextInt));
            busRealTime.setArrivingIn(random.nextInt(5) + 1);
            return busRealTime;
        } catch (Throwable t) {
            return null;
        }
    }
}
