package info.bus.sockets.client;

import info.bus.sockets.model.Bus;
import info.bus.sockets.model.BusRealTime;
import info.util.ErrorLogger;

import java.io.ObjectInputStream;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.HashMap;
import java.util.logging.Level;

public class BusInfoClient {
    static private Socket socket;
    static private PrintWriter writer;
    static private ObjectInputStream inputStream;

    static {
        try {
            socket = new Socket("localhost", 9001);
            inputStream = new ObjectInputStream(socket.getInputStream());
            writer = new PrintWriter(new OutputStreamWriter(socket.getOutputStream()), true);
        } catch (Throwable t) {
            ErrorLogger.log(Level.SEVERE, t.getMessage());
        }
    }

    static public HashMap getRoutes() throws Throwable {
        writer.println("ROUTES");
        return (HashMap) inputStream.readObject();
    }

    static public Bus getBusByNumber(int number) throws Throwable {
        writer.println("INFO#" + number);
        return (Bus) inputStream.readObject();
    }

    static public BusRealTime getRealTimeBus(int number) throws Throwable {
        writer.println("REAL_TIME#" + number);
        return (BusRealTime) inputStream.readObject();
    }

    static public void end() throws Throwable {
        writer.println("END");
        writer.close();
        inputStream.close();
        socket.close();
    }
}
