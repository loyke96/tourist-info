package info.bus.sockets.server;

import info.util.ErrorLogger;

import java.net.ServerSocket;
import java.net.Socket;
import java.util.logging.Level;

public class BusInfo {
    public static void main(String[] args) {
        try (ServerSocket serverSocket = new ServerSocket(9001)) {
            System.out.println("Server up");
            while (true) {
                Socket socket = serverSocket.accept();
                new BusInfoThread(socket).start();
            }
        } catch (Throwable t) {
            ErrorLogger.log(Level.SEVERE, t.getMessage());
        }
    }
}
