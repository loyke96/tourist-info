package info.bus.sockets.server;

import info.bus.sockets.util.BusUtil;
import info.util.ErrorLogger;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.util.logging.Level;

public class BusInfoThread extends Thread {
    private Socket socket;
    private BufferedReader reader;
    private ObjectOutputStream outputStream;

    public BusInfoThread(Socket socket) throws IOException {
        this.socket = socket;
        reader = new BufferedReader(new InputStreamReader(socket.getInputStream()));
        outputStream = new ObjectOutputStream(socket.getOutputStream());
    }

    @Override
    public void run() {
        try {
            String line;
            while (!(line = reader.readLine()).equals("END")) {
                if (line.equals("ROUTES")) {
                    outputStream.writeObject(BusUtil.getAllBusRoutes());
                } else if (line.startsWith("INFO#")) {
                    int busNumber = Integer.parseInt(line.split("#")[1]);
                    outputStream.writeObject(BusUtil.getBusByNumber(busNumber));
                } else if (line.startsWith("REAL_TIME#")) {
                    int busNumber = Integer.parseInt(line.split("#")[1]);
                    outputStream.writeObject(BusUtil.getRealTimeBusInfo(busNumber));
                }
            }
            outputStream.close();
            reader.close();
            socket.close();
        } catch (Throwable t) {
            t.printStackTrace();
            ErrorLogger.log(Level.SEVERE, t.getMessage());
        }
    }
}
