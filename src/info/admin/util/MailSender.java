package info.admin.util;

import info.util.ErrorLogger;

import javax.mail.*;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import java.io.FileReader;
import java.io.IOException;
import java.util.Properties;
import java.util.logging.Level;

public class MailSender {
    static private String mailTo;
    static private String mailFrom;
    static private String username;
    static private String password;
    static private Properties props;

    static {
        try {
            props = new Properties();
            props.load(new FileReader("res/mail.properties"));
            mailTo = props.getProperty("mailTo");
            mailFrom = props.getProperty("mailFrom");
            username = props.getProperty("username");
            password = props.getProperty("password");
        } catch (IOException e) {
            ErrorLogger.log(Level.SEVERE, e.getMessage());
        }
    }

    static public void send(String text) {
        Session session = Session.getInstance(
                props,
                new Authenticator() {
                    protected PasswordAuthentication getPasswordAuthentication() {
                        return new PasswordAuthentication(username, password);
                    }
                }
        );
        try {
            Message message = new MimeMessage(session);
            message.setFrom(new InternetAddress(mailFrom));
            message.setRecipients(Message.RecipientType.TO, InternetAddress.parse(mailTo));
            message.setSubject("Turist info: Greška");
            message.setText(text);
            Transport.send(message);
        } catch (MessagingException e) {
            ErrorLogger.log(Level.SEVERE, e.getMessage());
        }
    }
}
