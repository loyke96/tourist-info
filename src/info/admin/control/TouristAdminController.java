package info.admin.control;

import info.admin.model.ConnectedApp;
import info.admin.util.MailSender;
import info.util.ErrorLogger;
import javafx.application.Platform;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextArea;
import javafx.scene.control.cell.PropertyValueFactory;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.URL;
import java.time.Instant;
import java.util.ResourceBundle;
import java.util.logging.Level;

public class TouristAdminController implements Initializable {
    @FXML
    private TableView<ConnectedApp> appsTableView;
    @FXML
    private TextArea loggerTextArea;

    @FXML
    private void onShutDownAppClick() {
        ConnectedApp selectedItem = appsTableView.getSelectionModel().getSelectedItem();
        try {
            if (selectedItem != null) {
                selectedItem.shutDown();
                appsTableView.getItems().remove(selectedItem);
            }
        } catch (IOException e) {
            ErrorLogger.log(Level.SEVERE, e.getMessage());
        }
    }

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        ObservableList<TableColumn<ConnectedApp, ?>> columns = appsTableView.getColumns();
        columns.get(0).setCellValueFactory(new PropertyValueFactory<>("ipAddress"));
        columns.get(1).setCellValueFactory(new PropertyValueFactory<>("port"));
        columns.get(2).setCellValueFactory(new PropertyValueFactory<>("elapsedTime"));

        Thread waitForConnection = new Thread(() -> {
            try {
                ServerSocket serverSocket = new ServerSocket(9003);
                while (true) {
                    Socket socket = serverSocket.accept();
                    ConnectedApp app = new ConnectedApp(socket, Instant.now());
                    Platform.runLater(() -> appsTableView.getItems().add(app));
                }
            } catch (IOException e) {
                ErrorLogger.log(Level.SEVERE, e.getMessage());
            }
        });
        waitForConnection.setDaemon(true);
        waitForConnection.start();

        Thread calcElapsed = new Thread(() -> {
            while (true) {
                try {
                    ObservableList<ConnectedApp> apps = appsTableView.getItems();
                    apps.forEach(connectedApp -> {
                        try {
                            connectedApp.calculateElapsed();
                            connectedApp.logMessages(loggerTextArea);
                        } catch (IOException e) {
                            String errorDescription = "Aplikacija \"Turist info\" na " +
                                    connectedApp.getIpAddress() +
                                    " je prestala sa radom.";
                            new Thread(() -> MailSender.send(errorDescription)).start();
                            loggerTextArea.setText(loggerTextArea.getText() + errorDescription + System.lineSeparator());
                            Platform.runLater(() -> appsTableView.getItems().remove(connectedApp));
                        }
                    });
                    appsTableView.refresh();
                    Thread.sleep(1_000);
                } catch (InterruptedException e) {
                    ErrorLogger.log(Level.SEVERE, e.getMessage());
                }
            }
        });
        calcElapsed.setDaemon(true);
        calcElapsed.start();
    }
}
