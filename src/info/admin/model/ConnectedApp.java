package info.admin.model;

import info.util.ErrorLogger;
import javafx.application.Platform;
import javafx.scene.control.TextArea;

import java.io.*;
import java.net.Socket;
import java.time.Duration;
import java.time.Instant;
import java.util.logging.Level;

public class ConnectedApp {
    private Socket socket;
    private String ipAddress;
    private int port;
    private Instant startTime;
    private String elapsedTime;
    private PrintWriter writer;
    private BufferedReader reader;

    public ConnectedApp() {
    }

    public ConnectedApp(Socket socket, Instant startTime) {
        try {
            this.socket = socket;
            this.ipAddress = socket.getInetAddress().getHostAddress();
            this.port = socket.getPort();
            this.reader = new BufferedReader(new InputStreamReader(socket.getInputStream()));
            this.writer = new PrintWriter(new OutputStreamWriter(socket.getOutputStream()), true);
            this.startTime = startTime;
            this.elapsedTime = "00:00:00";
        } catch (IOException e) {
            ErrorLogger.log(Level.SEVERE, e.getMessage());
        }
    }

    public String getIpAddress() {
        return ipAddress;
    }

    public int getPort() {
        return port;
    }

    public Instant getStartTime() {
        return startTime;
    }

    public void calculateElapsed() {
        Duration duration = Duration.between(startTime, Instant.now());
        long seconds = duration.getSeconds();
        elapsedTime = String.format(
                "%02d:%02d:%02d",
                seconds / 3600,
                seconds / 60 % 60,
                seconds % 60
        );
    }

    public String getElapsedTime() {
        return elapsedTime;
    }

    public void shutDown() throws IOException {
        writer.println("Shut_Down");
        writer.close();
        reader.close();
        socket.close();
    }

    public void logMessages(TextArea loggerTextArea) throws IOException {
        writer.println();
        if (writer.checkError()) {
            throw new IOException();
        }
        if (reader.ready()) {
            String line = reader.readLine();
            Platform.runLater(() -> {
                String text = line + System.lineSeparator();
                Platform.runLater(() -> {
                    loggerTextArea.setText(loggerTextArea.getText() + socket.getInetAddress().getHostAddress()
                            + ": " + text);
                });
            });
        }
    }
}
