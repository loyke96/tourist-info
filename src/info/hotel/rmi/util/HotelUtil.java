package info.hotel.rmi.util;

import info.hotel.rmi.model.Hotel;

import java.util.List;

public class HotelUtil {
    static private List<Hotel> hotels;

    static {
        hotels = HotelSerializer.deserializeAll();
    }

    static public List<Hotel> getAllHotels() {
        return hotels;
    }

    static public Hotel getHotelByTitle(String title) {
        for (Hotel hotel : hotels) {
            if (hotel.getTitle().equals(title)) {
                return hotel;
            }
        }
        return null;
    }

    static public boolean addHotel(Hotel hotel) {
        for (Hotel hotel1 : hotels) {
            if (hotel1.getTitle().equals(hotel.getTitle())) {
                return false;
            }
        }
        hotels.add(hotel);
        HotelSerializer.serialize(hotel);
        return true;
    }

    static public boolean deleteHotel(String title) {
        for (Hotel hotel : hotels) {
            if (hotel.getTitle().equals(title)) {
                hotels.remove(hotel);
                HotelSerializer.deleteHotel(title);
                return true;
            }
        }
        return false;
    }
}
