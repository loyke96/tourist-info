package info.hotel.rmi.util;

import com.esotericsoftware.kryo.Kryo;
import com.esotericsoftware.kryo.io.Input;
import com.esotericsoftware.kryo.io.Output;
import com.google.gson.Gson;
import info.hotel.rmi.model.Hotel;
import info.util.ErrorLogger;

import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import java.util.logging.Level;

public class HotelSerializer {
    static private String folder;
    static private String type;
    static private Gson gson;
    static private Kryo kryo;

    static {
        try {
            gson = new Gson();
            kryo = new Kryo();
            kryo.register(Hotel.class);
            Properties properties = new Properties();
            properties.load(new FileReader("res/serialization.properties"));
            folder = properties.getProperty("FOLDER");
            type = properties.getProperty("TYPE");
        } catch (IOException e) {
            ErrorLogger.log(Level.SEVERE, e.getMessage());
        }
    }

    static public void serialize(Hotel hotel) {
        if (type.equals("Java")) {
            serializeJava(hotel);
        } else if (type.equals("Gson")) {
            serializeGson(hotel);
        } else if (type.equals("Kryo")) {
            serializeKryo(hotel);
        }
    }

    static private void serializeJava(Hotel hotel) {
        try (ObjectOutputStream outputStream = new ObjectOutputStream(new FileOutputStream(
                folder + File.separatorChar + hotel.getTitle() + ".ser"
        ))) {
            outputStream.writeObject(hotel);
        } catch (IOException e) {
            ErrorLogger.log(Level.SEVERE, e.getMessage());
        }
    }

    static private void serializeGson(Hotel hotel) {
        String json = gson.toJson(hotel);
        try (FileWriter writer = new FileWriter(folder + File.separatorChar + hotel.getTitle() + ".json")) {
            writer.write(json);
        } catch (IOException e) {
            ErrorLogger.log(Level.SEVERE, e.getMessage());
        }
    }

    static private void serializeKryo(Hotel hotel) {
        try {
            Output output = new Output(
                    new FileOutputStream(folder + File.separatorChar + hotel.getTitle() + ".kryo")
            );
            kryo.writeObject(output, hotel);
            output.close();
        } catch (FileNotFoundException e) {
            ErrorLogger.log(Level.SEVERE, e.getMessage());
        }
    }

    static public List<Hotel> deserializeAll() {
        ArrayList<Hotel> hotels = new ArrayList<>();
        File[] files = new File(folder).listFiles();
        assert files != null;
        for (File file : files) {
            try {
                if (file.getName().endsWith(".ser")) {
                    hotels.add(deserializeJava(file));
                } else if (file.getName().endsWith(".kryo")) {
                    hotels.add(deserializeKryo(file));
                } else if (file.getName().endsWith(".json")) {
                    hotels.add(deserializeGson(file));
                }
            } catch (IOException | ClassNotFoundException e) {
                ErrorLogger.log(Level.SEVERE, e.getMessage());
            }
        }
        return hotels;
    }

    static private Hotel deserializeJava(File file) throws IOException, ClassNotFoundException {
        ObjectInputStream inputStream = new ObjectInputStream(new FileInputStream(file));
        return (Hotel) inputStream.readObject();
    }

    static private Hotel deserializeGson(File file) throws FileNotFoundException {
        return gson.fromJson(new FileReader(file), Hotel.class);
    }

    static private Hotel deserializeKryo(File file) throws FileNotFoundException {
        Input input = new Input(new FileInputStream(file));
        Hotel hotel = kryo.readObject(input, Hotel.class);
        input.close();
        return hotel;
    }

    @SuppressWarnings("ResultOfMethodCallIgnored")
    static public void deleteHotel(String title) {
        new File(folder + File.separatorChar + title + ".ser").delete();
        new File(folder + File.separatorChar + title + ".json").delete();
        new File(folder + File.separatorChar + title + ".xml").delete();
    }
}
