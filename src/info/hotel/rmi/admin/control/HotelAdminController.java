package info.hotel.rmi.admin.control;

import info.hotel.rmi.model.Hotel;
import info.hotel.rmi.server.HotelInterface;
import info.util.ErrorLogger;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;

import java.net.URL;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.util.List;
import java.util.ResourceBundle;
import java.util.logging.Level;

public class HotelAdminController implements Initializable {
    @FXML
    private ListView<String> hotelsListView;
    @FXML
    private TextField titleTextField;
    @FXML
    private TextField addressTextField;
    @FXML
    private TextField phoneTextField;
    @FXML
    private TextField categoryTextField;
    private HotelInterface hotelInterface;

    @FXML
    private void onSelectHotel() {
        String selectedItem = hotelsListView.getSelectionModel().getSelectedItem();
        if (selectedItem != null) {
            try {
                Hotel hotel = hotelInterface.getHotelByTitle(selectedItem);
                titleTextField.setText(hotel.getTitle());
                addressTextField.setText(hotel.getAddress());
                phoneTextField.setText(hotel.getPhone());
                categoryTextField.setText(hotel.getCategory());
            } catch (RemoteException e) {
                ErrorLogger.log(Level.SEVERE, e.getMessage());
            }
        }
    }

    @FXML
    private void onDeleteHotelClick() {
        String selectedItem = hotelsListView.getSelectionModel().getSelectedItem();
        if (selectedItem != null) {
            try {
                hotelInterface.deleteHotel(titleTextField.getText());
                hotelsListView.getItems().remove(titleTextField.getText());
                clearFields();
            } catch (RemoteException e) {
                ErrorLogger.log(Level.SEVERE, e.getMessage());
            }
        }
    }

    @FXML
    private void onAddHotelClick() {
        try {
            hotelInterface.addHotel(new Hotel(
                    titleTextField.getText(),
                    addressTextField.getText(),
                    phoneTextField.getText(),
                    categoryTextField.getText()
            ));
            hotelsListView.getItems().add(titleTextField.getText());
            clearFields();
        } catch (RemoteException e) {
            ErrorLogger.log(Level.SEVERE, e.getMessage());
        }
    }

    private void clearFields() {
        titleTextField.clear();
        addressTextField.clear();
        phoneTextField.clear();
        categoryTextField.clear();
    }

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        System.setProperty("java.security.policy", "res/policy.txt");
        if (System.getSecurityManager() == null) {
            System.setSecurityManager(new SecurityManager());
        }
        try {
            Registry registry = LocateRegistry.getRegistry(1099);
            hotelInterface = (HotelInterface) registry.lookup("HotelInfo");
            List<Hotel> allHotels = hotelInterface.getAllHotels();
            for (Hotel hotel : allHotels) {
                hotelsListView.getItems().add(hotel.getTitle());
            }
        } catch (RemoteException | NotBoundException e) {
            ErrorLogger.log(Level.SEVERE, e.getMessage());
        }
    }
}
