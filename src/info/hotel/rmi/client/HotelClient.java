package info.hotel.rmi.client;

import info.hotel.rmi.model.Hotel;
import info.hotel.rmi.server.HotelInterface;
import info.util.ErrorLogger;

import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.util.List;
import java.util.logging.Level;

public class HotelClient {
    static private HotelInterface hotelInterface;

    static {
        System.setProperty("java.security.policy", "res/policy.txt");
        if (System.getSecurityManager() == null) {
            System.setSecurityManager(new SecurityManager());
        }
        try {
            Registry registry = LocateRegistry.getRegistry(1099);
            hotelInterface = (HotelInterface) registry.lookup("HotelInfo");
        } catch (RemoteException | NotBoundException e) {
            ErrorLogger.log(Level.SEVERE, e.getMessage());
        }
    }

    static public List<Hotel> getAllHotels() throws RemoteException {
        return hotelInterface.getAllHotels();
    }
}
