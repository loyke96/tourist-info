package info.hotel.rmi.server;

import info.hotel.rmi.model.Hotel;

import java.rmi.Remote;
import java.rmi.RemoteException;
import java.util.List;

public interface HotelInterface extends Remote {
    List<Hotel> getAllHotels() throws RemoteException;

    Hotel getHotelByTitle(String title) throws RemoteException;

    boolean addHotel(Hotel hotel) throws RemoteException;

    boolean deleteHotel(String title) throws RemoteException;
}
