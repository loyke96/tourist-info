package info.hotel.rmi.server;

import info.hotel.rmi.model.Hotel;
import info.hotel.rmi.util.HotelUtil;
import info.util.ErrorLogger;

import java.rmi.Remote;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;
import java.util.List;
import java.util.logging.Level;

public class HotelInfo implements HotelInterface {
    public static void main(String[] args) {
        System.setProperty("java.security.policy", "res/policy.txt");
        if (System.getSecurityManager() == null) {
            System.setSecurityManager(new SecurityManager());
        }
        try {
            Remote remote = UnicastRemoteObject.exportObject(new HotelInfo(), 0);
            Registry registry = LocateRegistry.createRegistry(1099);
            registry.rebind("HotelInfo", remote);
            System.out.println("Server up");
        } catch (RemoteException e) {
            ErrorLogger.log(Level.SEVERE, e.getMessage());
        }
    }

    @Override
    public List<Hotel> getAllHotels() {
        return HotelUtil.getAllHotels();
    }

    @Override
    public Hotel getHotelByTitle(String title) {
        return HotelUtil.getHotelByTitle(title);
    }

    @Override
    public boolean addHotel(Hotel hotel) {
        return HotelUtil.addHotel(hotel);
    }

    @Override
    public boolean deleteHotel(String title) {
        return HotelUtil.deleteHotel(title);
    }
}
