package info.hotel.rmi.model;

import java.io.Serializable;

public class Hotel implements Serializable {
    private String title;
    private String address;
    private String phone;
    private String category;

    public Hotel() {
    }

    public Hotel(String title, String address, String phone, String category) {
        this.title = title;
        this.address = address;
        this.phone = phone;
        this.category = category;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }
}
